<?php

namespace Application\Constants;

/**
 * Interface FormConst.
 */
interface ProdutoConst
{
    const DAO = 'ProdutoDao';
    const SERVICE = 'ProdutoService';

    const FLD_ID_PRODUTO = 'idProduto';
    const LBL_ID_PRODUTO = 'Id';


    const FLD_DESC_PRODUTO = 'descricaoProduto';
    const LBL_DESC_PRODUTO = 'Descricao';

    const FLD_VAL_UNITARIO = 'valorUnitario';
    const LBL_VAL_UNITARIO = 'Valor Unitario';

}
