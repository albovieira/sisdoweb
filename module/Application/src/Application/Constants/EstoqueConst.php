<?php

namespace Application\Constants;

/**
 * Interface FormConst.
 */
interface EstoqueConst
{
    const DAO = 'EstoqueDao';
    const SERVICE = 'EstoqueService';

    const FLD_ID_ESTOQUE = 'id';
    const LBL_ID_ESTOQUE = 'Id';


    const FLD_ID_PRODUTO = 'idProduto';
    const LBL_ID_PRODUTO = 'Produto';

    const FLD_QTD_PRODUTO = 'quantidade_estoque';
    const LBL_QTD_PRODUTO = 'Quantidade Estoque';

    const FLD_NOTA_FISCAL = 'nota_fiscal';
    const LBL_NOTA_FISCAL = 'Nota Fiscal';
}
