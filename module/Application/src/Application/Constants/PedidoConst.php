<?php

namespace Application\Constants;

/**
 * Interface FormConst.
 */
interface PedidoConst
{
    const DAO = 'PedidoDao';
    const SERVICE = 'PedidoService';

    const FLD_ID_PEDIDO = 'idPedido';
    const LBL_ID_PEDIDO = 'Id';


    const FLD_DATA = 'data';
    const LBL_DATA = 'Data';

    const FLD_VALOR_TOTAL = 'valorTotal';
    const LBL_VALOR_TOTAL = 'Valor Pedido';

    const FLD_FORNECEDOR = 'idFornecedor';
    const LBL_FORNECEDOR = 'Fornecedor';

    const FLD_STATUS = 'status';
    const LBL_STATUS = 'Status';
}
