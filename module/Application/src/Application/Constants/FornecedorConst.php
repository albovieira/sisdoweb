<?php

namespace Application\Constants;

/**
 * Interface FormConst.
 */
interface FornecedorConst
{
    const DAO = 'FornecedorDao';
    const SERVICE = 'FornecedorService';

    const FLD_ID_FORNEC = 'idFornecedor';
    const LBL_ID_FORNEC = 'Id';


    const FLD_NOME_FORNEC = 'nomeFornecedor';
    const LBL_NOME_FORNEC = 'Nome Fornecedor';

    const FLD_CNPJ = 'cnpj';
    const LBL_CNPJ = 'Cnpj';

}
